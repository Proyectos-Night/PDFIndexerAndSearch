package workflow;

public class Page implements Comparable<Page>{

	private String text;
	private Integer pageNumber;
	
	public Page(String text, Integer pageNumber) {
		this.text = text;
		this.pageNumber = pageNumber;
	}
	
	public void setPageNumber(Integer number) {
		this.pageNumber = number;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return this.text;
	}
	
	public Integer getPageNumber() {
		return this.pageNumber;
	}

	@Override
	public int compareTo(Page other) {
		if (this.pageNumber < other.pageNumber)
			return -1;
		if (this.pageNumber > other.pageNumber)
			return 1;
		return 0;
	}
}
