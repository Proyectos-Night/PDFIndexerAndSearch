package workflow;

import java.io.IOException;

import App.Input;
import App.Output;
import Framework.ConcreteClasses.ReaderPDF;
import Framework.ConcreteClasses.WriterPDF;

public class Master {

	private Document<Page> book = new Document<Page>();
	private IndexMaker indexer = new IndexMaker();
	private Input input = new Input(new ReaderPDF("Bible_King_James_Version.pdf"));
	private Output output = new Output(new WriterPDF("IndexMade.pdf"));
	
	public Master() {
		book.addObserver(indexer);
		runtest();
	}
	
	private void runtest() {
		try {
			input.read(book);
			output.write(book);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Master();
	}

}
