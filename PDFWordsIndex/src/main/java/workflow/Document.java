package workflow;

import java.util.ArrayList;
import java.util.List;

import Framework.AbstractClasses.AbstractStorageObservable;

@SuppressWarnings("hiding")
public class Document<E> extends AbstractStorageObservable<E> {

	private String title;
	private List<E> pages;
	private Index index;
	
	public Document() {
		title = null;
		pages = new ArrayList<E>();
		index = new Index();
	}
	
	@Override
	public void insert(E o) {
		pages.add(o);
        setChanged();
        notifyObservers(o);
	}

	@Override
	public void remove(E o) {
		pages.remove(o);
	}

	@Override
	public E get(Integer index) {
		return pages.get(index);
	}

	@Override
	public List<E> all() {
		return pages;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Index getIndex() {
		return this.index;
	}
	
	public String getTitle() {
		return this.title;
	}
}
