package workflow;

import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeSet;

public class IndexMaker implements Observer {

	@Override
	public void update(Observable arg0, Object arg1) {
		try {
			@SuppressWarnings("unchecked")
			Document<Page> currentBook = (Document<Page>) arg0;
			Page currentPage = (Page) arg1;
			classifyPageWords(currentPage.getText(), currentPage.getPageNumber(), currentBook.getIndex());}
		catch(Exception e) {e.printStackTrace();}
		
	}
	
	private void classifyPageWords(String currentPage, Integer page, Index index) {
		Arrays.stream(currentPage.toLowerCase().replace(";", " ").replace(":", " ").replace(".", " ").replace(",", " ").split("\\s")).forEach(word -> {
			if (word.endsWith(",") || word.endsWith(".")) 
				word = word.substring(0, word.length() - 2);
			if (index.getIndex().containsKey(word))
				index.addPageNumber(word, page);
			if (!index.getIndex().containsKey(word))
				index.getIndex().put(word, new TreeSet<>(Arrays.asList(page)));
		});
	}

}
