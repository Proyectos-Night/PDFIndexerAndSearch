package workflow;

import java.util.Comparator;
import java.util.Observable;
import java.util.TreeMap;
import java.util.TreeSet;

public class Index extends Observable {
	
	private TreeMap<String, TreeSet<Integer>> index = new TreeMap<>(Comparator.comparing(String::toLowerCase));
	
	public TreeMap<String, TreeSet<Integer>> getIndex(){
		return this.index;
	}
	
	public void addPageNumber(String key, Integer page) {
		if (index.containsKey(key))
			index.get(key).add(page);
	}
	
	public TreeSet<Integer> getListPages(String key){
		return this.index.get(key);
	}
}
