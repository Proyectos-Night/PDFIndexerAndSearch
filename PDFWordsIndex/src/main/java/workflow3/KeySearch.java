package workflow3;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import Framework.ConcreteClasses.ObservableStorage;

public class KeySearch implements Observer {

	private List<String> keywords = new ArrayList<>();
	private ObservableStorage<String> lines;
	
	public KeySearch(ObservableStorage<String> lines) {
		this.lines = lines;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		
		String path = (String) arg1;
		String split []= path.split("\\\\");
		String filename = split[split.length-1];
		
		if (!keywords.isEmpty()) {
			keywords.forEach(keyword -> {
				if (filename.toLowerCase().contains(keyword.toLowerCase()))
					lines.insert("Keyword: " + keyword + "  Filename:  " + filename + "  ->" + path);
				if (keyword.toLowerCase().contains(filename.toLowerCase()))
					lines.insert("Keyword: " + keyword + "  Filename:  " + filename + "  ->" + path);
			});
		}
	}
	
	public void setKeysWords(List<String> keywords) {
		this.keywords = keywords;
	}
}
