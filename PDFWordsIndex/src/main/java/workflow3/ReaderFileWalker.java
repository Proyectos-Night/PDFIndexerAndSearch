package workflow3;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import Framework.AbstractClasses.AbstractReader;
import Framework.interfaces.Storage;

public class ReaderFileWalker extends AbstractReader{

	private String path;
    @SuppressWarnings("unused")
	private File file;

    public ReaderFileWalker(String path){
        this.path = path;
        file = new File(path);

    }

    @SuppressWarnings("unchecked")
	@Override
    public <E> void read(Storage<E> lines) throws IOException {
        System.out.println("Text file leido"+ this.path);
        try {
        	Files.walk(Paths.get(path)).forEach(paths -> lines.insert((E) paths.toString()));
    		}
    		catch(IOException e) {
    		}
    }
}
