package workflow3;

import java.io.IOException;
import java.util.Arrays;

import App.Alphabetizer;
import App.Input;
import App.Output;
import Framework.ConcreteClasses.ObservableStorage;
import Framework.ConcreteClasses.WriterTextFile;

public class Master {

	private Input input = new Input(new ReaderFileWalker("C:\\Users\\Night Valbarz\\git"));
	private Output output = new Output(new WriterTextFile("resultsFiles.txt"));
	private ObservableStorage<String> lines = new ObservableStorage<String>();
	private Alphabetizer alpha = new Alphabetizer();
	
	private ObservableStorage<String> found = new ObservableStorage<String>();
	private KeySearch keysearch = new KeySearch(found);
	
	
	public Master() {
		lines.addObserver(keysearch);
		found.addObserver(alpha);
		test();
	}
	
	public void test() {
		keysearch.setKeysWords(Arrays.asList(".project",".java","graph","index", "git"));
		try {
			input.read(lines);
			output.write(found);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main (String arg[]) {
		new Master();
	}
}
