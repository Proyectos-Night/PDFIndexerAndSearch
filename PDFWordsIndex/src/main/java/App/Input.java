package App;

import java.io.IOException;

import Framework.interfaces.Reader;
import Framework.interfaces.Storage;

public class Input {

    private Reader reader;

    @SuppressWarnings("unused")
	private Input() {}
    
    public Input( Reader reader ){
        this.reader = reader;

    }

    public <E> void read(Storage<E> lines) throws IOException {
        this.reader.read(lines);
    }

}
