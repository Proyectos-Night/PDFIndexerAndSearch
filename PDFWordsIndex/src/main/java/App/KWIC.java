package App;

import java.io.IOException;

import Framework.ConcreteClasses.ObservableStorage;
import Framework.ConcreteClasses.ReaderTextFile;
import Framework.ConcreteClasses.WriterTextFile;

public class KWIC {

	private ObservableStorage<String> lines = new ObservableStorage<String>();
	private ObservableStorage<String> shifts = new ObservableStorage<String>();
	private Input input = new Input(new ReaderTextFile("keywords.txt"));
	private Output output = new Output(new WriterTextFile("output.txt"));

	private CircularShift circularShift = new CircularShift(shifts);
	private Alphabetizer alphabetizer = new Alphabetizer();

	public KWIC() {
		lines.addObserver(circularShift);
		shifts.addObserver(alphabetizer);
		run();
	}

	private void run() {
		try {
			input.read(lines);
			output.write(shifts);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new KWIC();
	}
}
