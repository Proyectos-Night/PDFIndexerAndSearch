package App;

import java.io.IOException;

import Framework.interfaces.Storage;
import Framework.interfaces.Writer;

public class Output {

    private Writer writer;

    public Output (Writer writer){
        this.writer = writer;
    }
    @SuppressWarnings("resource")
    public <E> void write(Storage<E> shifts) throws IOException{
        writer.write(shifts);
    }
}
