package App;

import Framework.ConcreteClasses.ObservableStorage;

import java.util.Comparator;
import java.util.Observable;
import java.util.Observer;

public class Alphabetizer implements Observer {

    @Override
    public void update(Observable o, Object arg1) {
        @SuppressWarnings("unchecked")
		ObservableStorage<String> shifts = (ObservableStorage<String>) o;
        shifts.all().sort(Comparator.comparing(String::toLowerCase));
    }
}

