package Framework.ConcreteClasses;

import java.util.ArrayList;
import java.util.List;

import Framework.AbstractClasses.AbstractStorage;

public class NormalStorage<E> extends AbstractStorage<E>{

	private List<E> lines = new ArrayList<>();

    public void insert(E line) {
        lines.add(line);
    }

    public void remove(E o) {
        lines.remove(o);
    }

    public E get(Integer index) {
        return lines.get(index);
    }

    public List<E> all(){
        return lines;
    }
	
}
