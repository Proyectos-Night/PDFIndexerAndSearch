package Framework.ConcreteClasses;

import java.awt.Color;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import Framework.AbstractClasses.AbstractWriter;
import Framework.interfaces.Storage;
import workflow.Document;
import workflow.Page;

public class WriterPDF extends AbstractWriter{

    private String path;

    public WriterPDF( String path ) {
        this.path = path;
    }

    @Override
    public <E> void write(Storage<E> document) {
        @SuppressWarnings("unchecked")
		Document<Page> doc = (Document<Page>) document;
        writePDF(doc.getIndex().getIndex(), path);
    }
    
    
    
    private final Integer FONTSIZE = 10;
	private final Integer MOVEMENT = -FONTSIZE - 2;
	private Integer CURSORPOSITION = 0;
    
    private void writePDF(TreeMap<String, TreeSet<Integer>> index, String docname) {

		try {
			
			PDDocument doc = new PDDocument();
			PDPage page = new PDPage();
			CURSORPOSITION = 700;
			doc.addPage(page);
			PDFont font = PDType1Font.COURIER_BOLD_OBLIQUE;
			Color color = Color.BLACK;
			PDPageContentStream contentStream = new PDPageContentStream(doc, page);
			contentStream.beginText();
			contentStream.setFont(font, FONTSIZE);
			contentStream.setNonStrokingColor(color);
			contentStream.newLineAtOffset(25, CURSORPOSITION + FONTSIZE + 2);

			for (Map.Entry<String, TreeSet<Integer>> line : index.entrySet()) {
				
				if (CURSORPOSITION < 100) {
					CURSORPOSITION = 700;

					contentStream.endText();
					contentStream.close();
					page = new PDPage();
					doc.addPage(page);
					
					contentStream = new PDPageContentStream(doc, page);
					contentStream.setFont(font, FONTSIZE);
					contentStream.beginText();
					contentStream.newLineAtOffset(25, CURSORPOSITION + FONTSIZE + 2);
				}
				contentStream.newLineAtOffset(0, MOVEMENT);
				CURSORPOSITION = CURSORPOSITION + MOVEMENT;
				contentStream.showText(line.getKey() + " " + line.getValue().toString());
			}

			contentStream.setNonStrokingColor(Color.BLACK);
			contentStream.endText();
			contentStream.close();
			doc.save(docname);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
