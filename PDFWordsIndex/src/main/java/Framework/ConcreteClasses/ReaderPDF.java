package Framework.ConcreteClasses;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import Framework.AbstractClasses.AbstractReader;
import Framework.interfaces.Storage;
import workflow.Page;


public class ReaderPDF extends AbstractReader {

    @SuppressWarnings("unused")
	private String path;
    private File file;

    public ReaderPDF(String path){
        this.path = path;
        file = new File(path);

    }

    @Override
    public <E> void read(Storage<E> lines) {
        try {
            extractPDFtoPages(lines, PDDocument.load(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
    @SuppressWarnings("unchecked")
	private <E> void extractPDFtoPages(Storage<E> book, PDDocument document) {
		PDFTextStripper stripper = null;
		for (int i = 0; i < document.getNumberOfPages(); i++) {
			try {
				stripper = new PDFTextStripper();
				stripper.setStartPage(i);
				stripper.setEndPage(i);
				String pageText = stripper.getText(document);
				book.insert((E) new Page(pageText, i));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
