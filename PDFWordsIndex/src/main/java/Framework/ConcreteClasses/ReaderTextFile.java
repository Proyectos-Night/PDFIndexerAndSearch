package Framework.ConcreteClasses;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import Framework.AbstractClasses.AbstractReader;
import Framework.interfaces.Storage;

public class ReaderTextFile extends AbstractReader {

    @SuppressWarnings("unused")
	private String path;
    private File file;

    public ReaderTextFile(String path){
        this.path = path;
        file = new File(path);

    }

    @SuppressWarnings("unchecked")
	@Override
    public <E> void read(Storage<E> lines) throws IOException {
        Files.readAllLines(Paths.get(file.getName())).forEach(line -> lines.insert((E) line));
    }

}
