package Framework.ConcreteClasses;

import Framework.AbstractClasses.AbstractWriter;
import Framework.interfaces.Storage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriterTextFile extends AbstractWriter {

    @SuppressWarnings("unused")
	private String path;
    private File file;

    public WriterTextFile(String path){
        this.path = path;
        this.file = new File(path);
    }

    @Override
    public <E> void write(Storage<E> shifts) throws IOException {
        @SuppressWarnings("resource")
		FileWriter writter = new FileWriter(file);
        shifts.all().stream().distinct().forEachOrdered(line -> {
            try {
                writter.write(line+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writter.flush();
    }
}
