package Framework.interfaces;

import java.io.IOException;

public interface Writer {

    public <E> void write(Storage<E> lines) throws IOException;

}
