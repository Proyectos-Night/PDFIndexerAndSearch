package Framework.interfaces;

import java.util.Collection;

public interface Storage<E> {

	public void insert(E o);
	public void remove (E o);
	public E get(Integer index);
	public Collection<E> all();
}
