package Framework.interfaces;

import java.io.IOException;

public interface Reader {

    public <E> void read(Storage<E> lines) throws IOException;
}
