package Framework.AbstractClasses;

import java.io.IOException;

import Framework.interfaces.Reader;
import Framework.interfaces.Storage;

public abstract class AbstractReader implements Reader {

    protected AbstractReader(){

    }

    public abstract <E> void read(Storage <E> lines) throws IOException;


}

