package Framework.AbstractClasses;

import java.io.IOException;

import Framework.interfaces.Storage;
import Framework.interfaces.Writer;

public abstract class AbstractWriter implements Writer{

    public abstract <E> void write(Storage<E> lines) throws IOException;
}
