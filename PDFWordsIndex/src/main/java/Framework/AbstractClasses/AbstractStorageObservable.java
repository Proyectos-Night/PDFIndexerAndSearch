package Framework.AbstractClasses;

import java.util.Observable;

import Framework.interfaces.Storage;

public abstract class AbstractStorageObservable<E> extends Observable implements Storage<E>{

}
