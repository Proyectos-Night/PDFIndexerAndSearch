package workflow2;

import java.io.IOException;

import App.Input;
import App.Output;
import Framework.ConcreteClasses.NormalStorage;
import Framework.ConcreteClasses.ReaderPDF;
import Framework.ConcreteClasses.ReaderTextFile;
import Framework.ConcreteClasses.WriterTextFile;
import Framework.interfaces.Storage;
import workflow.Document;
import workflow.Page;

public class Master {

	private Input input = new Input(new ReaderPDF("Bible_King_James_Version.pdf"));
	private Input inputKeywords = new Input(new ReaderTextFile("keywords.txt"));
	private Output output = new Output(new WriterTextFile("searchresults.txt"));
	
	private Document<Page> book = new Document<Page>();	
	private Storage<String> lines = new NormalStorage<String>();
	
	private PhraseSearch searcher = new PhraseSearch(lines);
    

	public Master() {
		book.addObserver(searcher);
		runtest();
	}

	private void runtest() {
		try {
			inputKeywords.read(lines);
			
			input.read(book);
			
			output.write(searcher.getStoragedResults());
			}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Master();
	}

}
