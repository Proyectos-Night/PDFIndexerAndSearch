package workflow2;

import java.util.Comparator;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;

import Framework.ConcreteClasses.NormalStorage;
import Framework.interfaces.Storage;
import workflow.Page;

public class PhraseSearch implements Observer {

	private Storage<String> keywords;
	private TreeMap<String,String> minindex = new TreeMap<String,String>(Comparator.comparing(String::toLowerCase));

	@SuppressWarnings("unused")
	private PhraseSearch() {
	}

	public PhraseSearch(Storage<String> keywords) {
		this.keywords = keywords;
	}

	@Override
	public void update(Observable arg0, Object line) {
		Page currentPage = (Page) line;
		
		keywords.all().stream().forEachOrdered(keyw -> {
			if (currentPage.getText().toLowerCase().contains(keyw.toLowerCase()))
				addSearchResult(keyw.toLowerCase(), currentPage.getPageNumber());
		});
	}

	private void addSearchResult(String phrase, Integer page) {
		if (minindex.containsKey(phrase))
			minindex.get(phrase).concat("," + page.toString());
		if (!minindex.containsKey(phrase))
			minindex.put(phrase, "->" + page.toString());
	}
	
	public Storage<String> getStoragedResults(){
		Storage<String> toReturn = new NormalStorage<>();
		minindex.entrySet().forEach(o -> toReturn.insert(o.getKey() + " " + o.getValue()));
		return toReturn;
	}
}
